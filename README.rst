Cookiecutter Dockerfile repository template
===========================================

Cookiecutter_ template for ESS ICS Docker_ image: ics-docker-<name>

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet::

    $ pip install cookiecutter

Generate a Dockerfile project::

    $ cookiecutter git+https://bitbucket.org/europeanspallationsource/cookiecutter-ics-docker

Detailed instructions
---------------------

The repository will be named `ics-docker-<name>`. `ics-docker` is automatically prepended.
Only enter `<name>` when asked for the docker_name.

To create `ics-docker-foo`::

    $ cookiecutter git+https://bitbucket.org/europeanspallationsource/cookiecutter-ics-docker
    company [European Spallation Source ERIC]:
    email [benjamin.bertrand@esss.se]:
    docker_name [ics-docker-<name>]: foo
    description [Dockerfile to build foo ESS Docker image]:
    image_name [europeanspallationsource/foo]:

CI0011906:~/Dev/cookiecutter $ tree ics-docker-foo/


This creates the following project::

    ics-docker-foo/
    ├── Dockerfile
    ├── LICENSE
    ├── Makefile
    └── README.rst


License
-------

BSD 2-clause license

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _Docker: https://www.docker.com
