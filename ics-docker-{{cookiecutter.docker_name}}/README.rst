{{ cookiecutter.description }}
=====================================

This Dockerfile creates a Docker_ image for .....


Usage
-----

After updating this repository, you should tag it::

    $ git tag -a 0.2.0

To be able to push the image to Docker Hub, you first have to login::

    $ docker login

You can then run::

    $ make push

This will create and push the image `{{ cookiecutter.image_name }}`.
The image is tagged with both `latest` and the current `git tag`.


.. _Docker: https://www.docker.com
