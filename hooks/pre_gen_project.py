import re
import sys

DOCKER_NAME_REGEX = r'^[a-z][a-z0-9\-]+$'

docker_name = '{{ cookiecutter.docker_name }}'

if not re.match(DOCKER_NAME_REGEX, docker_name):
    print('ERROR: "{}" is not a valid name! It should match "^[a-z][a-z0-9\-]+$"'.format(docker_name))
    sys.exit(1)
